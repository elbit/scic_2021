<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />

		<link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,400italic,600,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
		<script>
			$(document).ready(function() {
			    $('.menu').dropit();
			   
			});
		</script>

		<script src="<?php echo get_template_directory_uri(); ?>/js/lib/dropit.js"></script>

		<?php wp_head(); ?>

		<style>
			
		.menu-item a img, img.menu-image-title-after, img.menu-image-title-before, img.menu-image-title-above, img.menu-image-title-below, .menu-image-hover-wrapper .menu-image-title-above {
			    height: auto;
			    border: none;
			    box-shadow: none;
			    vertical-align: middle;
			    max-width:20px;
			    display: inline;
			}

			.menu ul.dropit-submenu {

				margin-top:0!important;
			}

			.menu ul.dropit-submenu:before {

				display:none;

			}

			.menu-item-has-children:after {

			/*position: absolute;*/
		    font-family: FontAwesome;
		    /*margin-top: -20px;*/
		    color: #69707a;
		    content: "\f0d7";
		    width: 100%;
		    height: 25px;



			}
			.menu-item-has-children {
				cursor: pointer;
			}

			.page-template-page-slim h1{
				margin-bottom: 2rem!important;
				font-size: 50px!important;
			}
			.page-template-page-slim h2{
				
				font-size: 30px!important;
			}
			.page-template-page-slim ul{
				margin-left: 20px;
			}
		</style>
		
</head>
	<body <?php body_class(); ?> id="up">

		<!-- wrapper -->
		<div class="wrapper">

	

		<!-- header -->
			<header role="banner" class="site-header">
				
				<div class="nav" role="navigation">
					
					<!-- logo -->
					<div class="nav-left logo" >
						
						<a  href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" class="logo-img">
						</a>
						
					</div>

					<div class="nav-center" >
						<?php html5blank_nav(); ?>
					</div>

					<div class="nav-right">
						
						<?php wp_nav_menu( array(
								   'extra-menu' => __('Extra Menu', 'html5blank')
								) ); ?>
						
						<div class="idiomes" style="display: flex; justify-content: flex-end;">
							
							
							<?php qtranxf_generateLanguageSelectCode() ?>

							<a class="eng" href="english" hreflang="en" title="English" >Eng</a>		
							
							
							
						
						</div>
					</div>

				</div>
					
			</header>


			<!-- /header -->