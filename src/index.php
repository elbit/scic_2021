<?php get_header(); ?>

<main role="main" class="main">
		
		<section class="destacats noticies">
		
		<?php 
		// WP_Query arguments
		$args = array (
			 
			'posts_per_page'         => '12',
			 
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
		<div <?php post_class('noticia'); ?>>

			<!-- <span class="noticia-category">
				<?php // the_category(' / '); ?>
			</span> -->
				<span class="category-badge"></span>
			<div class="noticia-img">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('custom-size'); ?></a>
			</div>


					
			<div class="noticia-text">
				
				<h2 class="noticia-header">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
			</div>
			
		</div> <!-- End Bloc Noticia -->
			
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p></p>
			<?php endif; ?>

		<!-- <div class="read-more">
			<a class="button button-large button-green button-read-more" href="/noticies/">Més noticies</a>

		</div> -->

			
	</section><!-- final noticies -->

</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
