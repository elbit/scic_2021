// npm install gulp-sass gulp-minify-css gulp-livereload gulp-autoprefixer --save-dev

var gulp = require('gulp');
//plugins
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
//var browserSync = require('browser-sync');
var livereload = require('gulp-livereload');


// TASKS

gulp.task('sass', function(){
  return gulp.src('sass/style.scss')
    .pipe(sass()) // Using gulp-sass
    .pipe(autoprefixer('last 3 versions'))
	.pipe(minifyCSS())
	.pipe(gulp.dest('../'))
  .pipe(livereload());
    
});



// Gulp watch 
gulp.task('watch',function(){
  livereload.listen();
  gulp.watch('sass/**/*.scss', ['sass']); 
  // Other watchers
  //gulp.watch('*.php'); 
})





