<?php 


get_header(); ?>

	<main role="main" class="page-wrapper <?php the_title(); ?>">
		
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class('content'); ?>>
			
			<?php //edit_post_link(); ?>
			
			<h2 class="page-title">
				<?php the_title(); ?> page title
			</h2>
			
			<?php the_content(); ?>

			

		</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
		<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		</article>
			<!-- /article -->

		<?php endif; ?>

		
	</main>

 <?php // get_sidebar(); ?>

<?php get_footer(); ?>
