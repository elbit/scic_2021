<?php /* Template Name: HOME */ get_header(); ?>

<main role="main" class="container">
		
	<section class="columns banners">

		<div class="column banner image">
			
			<a href="http://www.scic.cat/es/inscriu-la-teva-coral/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-2.png">
			</a>
		
		</div>

		<div class="column banner image ">
			
			<a href="http://www.scic.cat/sigues-amic-del-scic/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-1.png">
			</a>

		</div>

			<div class="column banner image ">
			
			<a href="http://www.scic.cat/sigues-amic-del-scic/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-1.png">
			</a>

		</div>
			

	</section>
		
	<div class="destacats columns">
			

		<?php 
		// WP_Query arguments
		$args = array (
			 
			'post__in' => get_option( 'sticky_posts' ),
			 
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>

			

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
		<div class="column is-equal-heigth">
				
			<div  <?php post_class('noticia has-box-shadow'); ?>>
					
				<div class="noticia-img">
						<?php the_post_thumbnail('custom-size'); ?>
				</div>
					
				<span class="noticia-category">
						<?php the_category(' / '); ?>
				</span>
					
				<div class="noticia-text">
						
					<h2 class="noticia-header">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
						
				</div>
			</div>
		</div> <!-- End Bloc Noticia -->
			
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p></p>
			<?php endif; ?>

		<div class="read-more">
			<a class="button is-read-more is-success is-outlined is-medium button-news" href="http://www.scic.cat/noticies/">Més noticies</a>
		</div>

			
	</div><!-- final noticies -->
		
	<section class="home-calendar-wrapper">
			
			<?php get_sidebar(''); ?> <!-- Agenda -->
		<div class="read-more">
			<a class="button is-read-more is-primary is-outlined is-medium button-agenda" href="/agenda">Agenda</a>
		</div>
		
	</section>
		
<!-- <section class="section box tenda container">

		<?php //echo do_shortcode('[recent_products per_page="12" columns="4"]');?>
			
	</section> -->
		<!-- final botiga -->
		
</main>

<?php get_footer(); ?>
