		</div> <!-- End wrapper -->

			<!-- footer -->
			<footer class="footer" role="contentinfo">

				
					<a href="#up" class="up-button"></a>
			

				<div>
					<p class="copyright">
						&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
						/ <a href="mailto:scic@scic.cat"> <i class="fa fa-envelope-o"> </i>  scic@scic.cat </a> <a href="politica-de-privacitat/" class="u-bit-text-nowrap">/ Política de Privacitat</a>
					</p>
				</div>

				<div class="nav-center" >
						<?php html5blank_nav(); ?>
						
					</div>
				

				<div class="footer-logos">
					
					<a href="http://mcc.cat"><img src="http://imscdn.abcore.org/r/mcc.cat/w245-h156/mm/image/logo_MCC.JPG" alt="mcc.cat" style="max-width:100px;"></a>
					<a href="http://cultura.gencat.cat"><img src="http://cultura.gencat.cat/web/resources/webgencat/comuns/img/logo_generalitat_gris.png_679097835.png" alt="" style="max-width:100px;"></a>

				</div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-80588039-1', 'auto');
		ga('send', 'pageview');
		</script>

	
	</body>
</html>
