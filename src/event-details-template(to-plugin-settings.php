<span class="event-time value-title" title="{dtstart}">{time} hola<span class="time-separator"> - </span>
<span class="end-time value-title" title="{dtend}">{endtime}</span></span>

<div class="sub-details">
{hcard}
<div class="mc-description">{image}{description}</div>
<p>{ical_html} • {gcal_link}</p>
{map}
<p><a href="{linking}" class="event-link external"><span class="screen-reader-text">More information about </span>{title}</a></p></div>



<h2 class="event-time value-title" title="{dtstart}">{title}</h2>

{image}

<h3>{date} / {time}</h3>
<p>{shortdesc}</p>
<p> <strong>{location} / {street} / {city} / {link_map}</strong></p>
<p><a href="{link}">Més informació</a></p>
<p>Categoria: {category}</p>
