<?php /* Template Name: principal*/ get_header(); ?>


	<main role="main" class="container">

		<section class="section columns banners ">

			<div class="column banner image ">
				<img src="http://placehold.it/640x120">
			</div>

			<div class="column banner image ">
				<img src="http://placehold.it/640x120">
			</div>
			

		</section>
		
		<section class="destacats columns">
			

		<?php 
		// WP_Query arguments
		$args = array (
			'cat'                    => '74',
			'posts_per_page'         => '3',
			'orderby'                => 'rand',
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>

			

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
			<div class="column is-equal-heigth" style="">
				<div  <?php post_class('noticia'); ?>>
					
					<div class="noticia-img">
					<?php the_post_thumbnail(); ?>
					</div>
					
					<span class="noticia-category">
						<?php the_category('/'); ?>
					</span>

					<div class="noticia-text">
						
						

						<h2 class="noticia-header">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		

		
		</section>


		<?php get_sidebar(); ?>
		
		

		<section class="noticies columns">
			
			<section class="columns is-multiline  ">
			<?php rewind_posts(); ?>

		<?php 
		// WP_Query arguments
		$args = array (
			
			'posts_per_page'         => '7',
			'orderby'                => 'rand',

			);

		
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>

			

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
			<div class="column is-equal-heigth is-one-quarter " style="">
				<div  <?php post_class('noticia'); ?>>

				<?php echo do_shortcode('[ajax_load_more]');?>
					
					<div class="noticia-img">
					<?php the_post_thumbnail(); ?>
					</div>
					
					<span class="noticia-category">
						<?php the_category('/'); ?>
					</span>

					<div class="noticia-text">
						
						

						<h2 class="noticia-header">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			

			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

		

		
		
		

		</section> <!-- final noticies -->

		<section class="tenda container">
			

		</section>
		
	
	</main>



<?php get_footer(); ?>
