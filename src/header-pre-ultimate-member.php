<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />

		<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,400italic,500,600,700,800,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

        

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">



		<!-- header -->
			<header class="nav" role="banner">

					<!-- logo -->
					<div class="logo nav-left">
						
						<a  href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Logo" class="logo-img">
						</a>
						
					</div>
					<!-- /logo -->

					<!-- nav -->
					<div class=" " role="navigation">
						<?php html5blank_nav(); ?>
						
					</div>
					<!-- /nav -->

					<div class="nav-right">

						
						<span class="nav-item">
						
						<a class="button" href="http://localhost:8888/SCIC/p1/wp-admin/">
							<span class="icon">
          					<i class="fa fa-user"> </i>
        				</span>
							  Login</a>
						</span>
					<!-- <?php 
global $user_login, $current_user; 

if (is_user_logged_in()) {
    get_currentuserinfo();
    $user_info = get_userdata($current_user->ID);
    if (in_array('contributor', $user_info->roles)) { 
?>    
<span class="nav-item"> Docs</span>
<span class="nav-item"> Agenda</span>
<?php
    }
}
?>    -->
				   
					</div>

			</header>
			<!-- /header -->
