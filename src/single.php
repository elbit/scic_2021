<?php get_header(); ?>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<main role="main" <?php post_class('article-wrapper' . " content "); ?>>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" >
			
			
			
			<?php //edit_post_link(); ?>

	
			<?php if ( has_post_thumbnail()) : ?>
				<a class="article-thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			<?php endif; ?>
			
			

			<h2 class="article-title">
				<?php the_title(); ?>
			</h2>
			
			
			
			

			<time class="article-date" datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
					<?php the_date(); ?> 
			</time>
			
			<?php the_content(); // Dynamic Content ?>

			
			<?php // the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

			<p><?php //_e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			<p><?php //_e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>

			

			<?php //comments_template(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	
	<!-- /section -->
	</main>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>
