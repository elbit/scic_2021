<?php /* Template Name: HOME */ get_header(); ?>

	
	<main role="main" class="container">
		
		<section class="columns banners">

			<div class="column banner image">
				<img src="http://placehold.it/640x100">
			</div>

			<div class="column banner image ">
				<img src="http://placehold.it/640x100">
			</div>
			

		</section>
		
		<section class="destacats columns is-multiline">
			

		<?php 
		// WP_Query arguments
		$args = array (
			// 'cat'                    => '125',
			'posts_per_page'         => '3',
			 'post__in' => $sticky,
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>

			

			<!-- the loop -->
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
			<div class="column   is-equal-heigth" style="">
				<div  <?php post_class('noticia has-box-shadow'); ?>>
					
					<div class="noticia-img">
					<?php the_post_thumbnail(); ?>
					</div>
					
					<span class="noticia-category">
						<?php the_category(' / '); ?>
					</span>

					<div class="noticia-text">
						
						

						<h2 class="noticia-header">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p></p>
			<?php endif; ?>

			
		</section><!-- final noticies -->
		
			<div class="columns">
			<a class="button read-more-button column is-one-third is-offset-4 is-success">Més noticies</a>
		</div>
			
		
		<?php get_sidebar(''); ?>

		<div class="columns">
			<a class="button read-more-button column is-one-third is-offset-4 is-warning">Agenda</a>
		</div>
	
		<section class="section box tenda container">

		<?php echo do_shortcode('[recent_products per_page="12" columns="4"]');?>
			
			
		</section><!-- final botiga -->
		
		<div class="columns">
			<a class="button read-more-button column is-one-third is-offset-4 is-primary">Agenda</a>
		</div>
	</main>



<?php get_footer(); ?>
