<!-- search -->
<form class="form menu-item__search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="input search-input" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
	<button class="button search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
</form>
<!-- /search -->
