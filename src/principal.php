<?php /* Template Name: HOME */ get_header(); ?>

<main role="main" class="main">
		
	<section class="banners">

	<div class="banner">
		<a href="https://www.scic.cat/55e-aniversari/" >
		<img src="<?php echo get_template_directory_uri(); ?>/img/55-aniversari.jpg">
		</a>
	</div>

		<div class="banner">

			  <?php if(qtranxf_getLanguage()=='ca'): ?>
		      	<a href="http://www.scic.cat/inscriu-la-teva-coral/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-2.png">
				</a>
		      <?php endif; ?>
		      
		      <?php if(qtranxf_getLanguage()=='es'): ?>
		       	<a href="http://www.scic.cat/inscriu-la-teva-coral/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-2-esp.svg">
				</a>
		      <?php endif; ?>

			
			
		
		</div>

		<div class="banner">

			 <?php if(qtranxf_getLanguage()=='ca'): ?>
		      	<a href="http://www.scic.cat/sigues-amic-del-scic/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-1.png">
				</a>
				
		      <?php endif; ?>
		      
		      <?php if(qtranxf_getLanguage()=='es'): ?>
		       	<a href="http://www.scic.cat/sigues-amic-del-scic/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/SCIC-banner-1-esp.svg">
				</a>
		      <?php endif; ?>
			
			
		</div>

		
	
	</section><br>

	<section class="banners">
		
		<div class="banner">

			<?php if(qtranxf_getLanguage()=='ca'): ?>
		      	<a href="http://botigueta.scic.cat/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-botigueta.png">
				</a>
				
		      <?php endif; ?>
		      
		      <?php if(qtranxf_getLanguage()=='es'): ?>
		       <a href="http://botigueta.scic.cat/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-botigueta.png">
				</a>
		      <?php endif; ?>
			
			
		</div>

		<div class="banner">

			<?php if(qtranxf_getLanguage()=='ca'): ?>
		      	<a href="curs-de-direccio">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-curs-direccio.jpg">
				</a>
				
		      <?php endif; ?>
		      
		      <?php if(qtranxf_getLanguage()=='es'): ?>
			       <a href="curs-de-direccio">
					<img src="<?php echo get_template_directory_uri(); ?>/img/banner-curs-direccio-esp.jpg">
				</a>
		      <?php endif; ?>
			
			
		</div>

	</section>


	<section class="destacats">
		
		<?php 
		// WP_Query arguments
		$args = array (
			 
			'post__in' => get_option( 'sticky_posts' ),
			 
			);
		// the query
		$the_query = new WP_Query( $args ); ?>

		<?php if ( $the_query->have_posts() ) : ?>
		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
		<div <?php post_class('noticia'); ?>>

			<span class="category-badge"></span>

			<!-- <span class="noticia-category">
				<?php //the_category(' / '); ?>
			</span> -->
				
			<div class="noticia-img">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('custom-size'); ?></a>
			</div>


					
			<div class="noticia-text">
				
				<h2 class="noticia-header">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
			</div>
			
		</div> <!-- End Bloc Noticia -->
			
			
			<?php endwhile; ?>
			<!-- end of the loop -->

			<?php wp_reset_postdata(); ?>

			<?php else : ?>
				<p></p>
			<?php endif; ?>

		<div class="read-more">


			<?php if(qtranxf_getLanguage()=='ca'): ?>
		      	<a class="button button-large button-green button-read-more" href="/noticies">Més noticies</a>
				
		      <?php endif; ?>
		      
		      <?php if(qtranxf_getLanguage()=='es'): ?>
		       	<a class="button button-large button-green button-read-more" href="/noticies">Más noticias</a>
		      <?php endif; ?>
			
			
			

		</div>

			
	</section><!-- final noticies -->
		
	<div class="home-calendar-wrapper">
			
			<?php get_sidebar(''); ?> <!-- Agenda -->
		
		<div class="read-more">
			<a class="button button-large button-blue button-read-more" href="/agenda">Agenda</a>

		</div>
		
	</div>

	<div class="xarxes">
		
		<?php get_sidebar('xarxes'); ?>

	</div>
		
<!-- <section class="section box tenda container">

		<?php //echo do_shortcode('[recent_products per_page="12" columns="4"]');?>
			
	</section> -->
		<!-- final botiga -->
		
</main>



<?php get_footer(); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
