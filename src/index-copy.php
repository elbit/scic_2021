<?php get_header(); ?>

	<main role="main" class="container">
		<!-- section -->
		<section class="section columns is-multiline">

			

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
